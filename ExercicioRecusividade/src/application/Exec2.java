package application;

import java.util.Scanner;

public class Exec2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
//		2 pow 2 = 4 porque 2 * 2 = 4
//		2 pow 3 = 8 porque 2 * 2 * 2 = 8
		
		System.out.print("Informe a potencia: ");
		int potencia = sc.nextInt();
		
		System.out.print("Informe a base: ");
		int base = sc.nextInt();
		
		double result=1;
		
		for (int i = 0; i < potencia; i++) {
			result = potencia(base, i);
		}
		System.out.println(result);
		
		sc.close();

	}

	public static double potencia(int n, int po) {

		if (po == 0)
			return 1;
		else if (po == 1)
			return n;
		else
			return potencia(n,po-1) * n;

	}

}
